import 'package:intl/intl.dart';

class Photo {
  final int likes;
  final String url;
  final String createdDate;

  Photo({
    this.likes,
    this.url,
    this.createdDate,
  });

  factory Photo.formJson(Map<String, dynamic> json){
    return Photo(
      likes: json['likes'],
      url: json['urls']['regular'],
      createdDate: DateFormat('dd.MM.yyyy').format(DateTime.parse(json['created_at'])),
    );
  }
}
