import 'dart:convert';

import 'package:block_practice_3/http/photo.dart';
import 'package:http/http.dart' as http;

class Repository{
  static const String _request = 'https://api.unsplash.com/photos?per_page=16&client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9';

  Repository._();

  static Future<List<Photo>> fetchPhotos() async {
    return jsonDecode((await http.get(_request)).body).map((item) => Photo.formJson(item)).toList().cast<Photo>().toList();
  }
}