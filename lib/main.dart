import 'package:block_practice_3/bloc.dart';
import 'package:block_practice_3/second_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: BlocProvider(
        create: (_) => PhotosBlock(),
        child: Builder(
          builder: (ctx) => Scaffold(
            appBar: AppBar(
              title: Text('Material App Bar'),
              actions: [
                IconButton(
                  icon: Icon(Icons.file_download),
                  onPressed: () => ctx.read<PhotosBlock>().add(FetchPhotosEvent()),
                ),
              ],
            ),
            body: StreamBuilder<PhotosState>(
              stream: ctx.read<PhotosBlock>(),
              builder: (_, snapshot) => GridView(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
                ),
                children: snapshot?.data?.photos
                        ?.map(
                          (item) => InkWell(
                            onTap: () => Navigator.push(
                              ctx,
                              MaterialPageRoute(
                                builder: (_) => SecondPage(
                                  photo: item,
                                ),
                              ),
                            ),
                            child: Image.network(
                              item.url,
                              height: 100,
                              width: 100,
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                        ?.toList() ??
                    [],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
