import 'package:block_practice_3/http/photo.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  final Photo photo;

  const SecondPage({Key key, this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(photo.createdDate),
            Text('${photo.likes} likes'),
          ],
        ),
      ),
      body: Center(
        child: Image.network(
          photo.url,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
