import 'package:block_practice_3/http/photo.dart';
import 'package:block_practice_3/http/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class PhotoEvent{}
class FetchPhotosEvent extends PhotoEvent{}

class PhotosState {
  final List<Photo> photos;

  PhotosState({this.photos});
}

class PhotosBlock extends Bloc<PhotoEvent, PhotosState>{
  PhotosBlock() : super(PhotosState(photos: []));



  @override
  Stream<PhotosState> mapEventToState(PhotoEvent event) async* {
    switch(event.runtimeType){
      case FetchPhotosEvent:
        yield PhotosState(photos: await Repository.fetchPhotos());
        break;
    }
  }
}
